import { useState, useContext, useEffect } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import { SyncOutlined } from "@ant-design/icons";
import Link from 'next/link';
import { Context } from "../context";
import { useRouter } from "next/router";

const ForgotPassword = () => {
    //state
    const [email, setEmail] = useState('');
    const [success, setSuccess] = useState(false);
    const [code, setCode] = useState('');
    const [newPassword, setNewPassoword] = useState('');
    const [loading, setLoading] = useState(false);

    //context
    const {state: {user}} = useContext(Context);
    //router
    const router = useRouter();

    //redirect if user is logged in
    useEffect(() => {
        if(user !== null) router.push('/');
    },[user]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try{
            setLoading(true);
            const {data} = await axios.post(`/api/forgot-password`, {email});
            setSuccess(true);
            toast('Check your email for the secret code');
            setLoading(false);
        }catch(err){
            setLoading(false)
            toast.error(err.response.data);
        }
    }

    const handleResetPassword = async (e) => {
        e.preventDefault();
        // console.log(email, code, newPassword);
        // return;
        try{
            setLoading(true);
            const {data} = await axios.post(`/api/reset-password`,{
                email, code, newPassword
            });
            setCode('');
            setEmail('');
            setNewPassoword('');
            setLoading(false);
            toast('Great! Now you can login with your new password');
        }catch(err){
            setLoading(false)
            toast.error(err.response.data);
        }
    }

    return(
        <div className="forgot-password-container">
            <div className="forgot-password-content col-md-4 offset-4 p-4">
            <h1 className="login-heading-primary mb-2  pl-3">Forgot Password<span className="login-span-blue">.</span></h1>
                <p className="text-mute mb-2 pl-3">Enter your email to reset your password.</p>
                <div className="container pb-4">
                    <form onSubmit={success ? handleResetPassword : handleSubmit}>
                        <input type='email' className="form-control mb-4 p-4" value={email} onChange={(e) => setEmail(e.target.value)} placeholder='Enter email' required/>
                        {success && <>
                            <input type='text' className="form-control mb-4 p-4" value={code} onChange={(e) => setCode(e.target.value)} placeholder='Enter secret code' required/>
                            <input type='password' className="form-control mb-4 p-4" value={newPassword} onChange={(e) => setNewPassoword(e.target.value)} placeholder='New password' required/>
                        </>}
                        <button type="submit" className="btn btn-primary btn-block p-2" disabled={loading || !email}>{loading ? <SyncOutlined spin/> : "Submit"}</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ForgotPassword;
