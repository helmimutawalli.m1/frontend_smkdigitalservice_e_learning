import {useState, useContext, useEffect} from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { SyncOutlined } from '@ant-design/icons';
import Link from 'next/link';
import {Context} from '../context';
import {useRouter} from 'next/router'
import {GoogleLogin} from 'react-google-login';

const Login = () => {
    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');
    const [loading,setLoading] = useState(false);

    //state
    const {state: {user}, dispatch} = useContext(Context);
    // const {user} = state;

    //router
    const router = useRouter();

    useEffect(() => {
        if(user !== null) router.push('/');
    },[user])

    const handleSubmit = async (e) => {
        e.preventDefault();
        // console.table({email, password});
        try{
            setLoading(true);
            const{data} = await axios.post(`/api/login`,{email,password});
            dispatch({
                type: "LOGIN",
                payload: data,
            });
            //save in local storage
            window.localStorage.setItem('user', JSON.stringify(data));
            //redirect
            router.push('/user');
            setLoading(false);
        }catch(err){
            toast.error(err.response.data);
            setLoading(false);
            setPassword('');
        }
    };

    const  handleGoogleLogin = async (googleData) => {
        try{
            setLoading(true);
            if(googleData.tokenId != undefined){
                const{data} = await axios.post(`/api/login/google`,{token: googleData.tokenId});

                dispatch({
                    type: "LOGIN",
                    payload: data,
                });
                //save in local storage
                window.localStorage.setItem('user', JSON.stringify(data));
                //redirect
                router.push('/user');
            }
            setLoading(false);
        }catch(err){
            toast.error(err.response.data);
            setLoading(false);
            setPassword('');
        }
    }

    const googleClientId = process.env.GOOGLE_CLIENT_ID;

    return(
        <div className='login-container'>
            <div className='login-content col-md-4 offset-4 p-4'>
                {/* <h1 className="text-center square">Log in</h1> */}

                <h1 className="login-heading-primary mb-2  pl-3">Log in<span className="login-span-blue">.</span></h1>
                <p className="text-mute mb-2 pl-3">Enter your credentials to access your account.</p>
                
                <div className="container pb-4">
                    <form onSubmit={handleSubmit}>
                        <input type='email' className="form-control mb-4 p-4" value={email} onChange={e => setEmail(e.target.value)} placeholder="Enter email" required/>
                        <input type='password' className="form-control mb-4 p-4" value={password} onChange={e => setPassword(e.target.value)} placeholder="Enter password" required/>
                        <button type='submit' className='btn btn-block btn-primary col-md-12' disabled={!email || !password || loading} >{loading ? <SyncOutlined spin /> : "Submit"}</button>
                    </form>
                    <div className='row col-md-12 m-0 p-0'>
                        <p className='text-center pt-3'>
                            Not yet registered?{" "}
                            <Link href='/register'><a>Register</a></Link>
                        </p>
                        <p className='text-center pt-3 forgot-password'>
                            <Link href='/forgot-password'><a className='text-danger'>Forgot password</a></Link>
                        </p>
                    </div>
                    <div className="line-breaker mb-2">
                        <span className="line"></span>
                        <span>or</span>
                        <span className="line"></span>
                    </div>    
                    <GoogleLogin 
                        className='btn btn-block btn-primary col-md-12'
                        clientId='227763611443-c4gbpoci2s7smja3n1h4tdquvjl8onj8.apps.googleusercontent.com'
                        buttonText="Log in with Google"
                        onSuccess={handleGoogleLogin}
                        onFailure={handleGoogleLogin}
                        cookiePolicy={'single_host_origin'}
                    />

                </div>
            </div>
        </div>
    )
}

export default Login;