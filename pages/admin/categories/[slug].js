import {useContext,useEffect, useState} from 'react';
import {Context} from '../../../context';
import AdminRoute from '../../../components/routes/AdminRoute';
import axios from 'axios';
import {Avatar, List, Button, Modal} from 'antd';
import Link from 'next/link';
import {SyncOutlined, DeleteOutlined, EditOutlined} from '@ant-design/icons'
import CategoryForm from '../../../components/forms/CategoryForm';
import {toast} from 'react-toastify';
import { useRouter } from "next/router";

const {Item} = List;

const SubCategories = () => {

    //router
    const router = useRouter();
    const {slug} = router.query;

    const {state: {user},} = useContext(Context);

    const [loading, setLoading] = useState(false);
    const [visibleAddSubCategory, setVisibleAddSubCategory] = useState(false);
    const [visibleUpdateSubCategory, setVisibleUpdateSubCategory] = useState(false);
    const [current, setCurrent] = useState({});
    const [subCategories, setSubCategories] = useState({subCategory:[]});
    const [values, setValues] = useState({
        name:''
    });

    useEffect(() => {
        const fetchSubCategory = async () => {
            if(slug){
                const {data} = await axios.get(`/api/sub-category/${slug}`);
                if(data) setSubCategories({subCategory:[...data]});
            }
        }
        fetchSubCategory();
    },[slug]);

    const handleAddSubCategory = async (e) => {
        e.preventDefault();
        // console.log(values);
        try{
            const {data} = await axios.post(`/api/sub-category/${slug}`, values);
            if(data) setSubCategories({subCategory:[...subCategories.subCategory,data]});
            setValues({name:''});
            setVisibleAddSubCategory(false);
            toast('Sub Category added');
        } catch(err){
            console.log(err);
            toast('Sub Category add failed');
        }
    }

    const handleUpdateSubCategory = async (e) => {
        e.preventDefault();
        try{
            const {data} = await axios.put(`/api/sub-category/${current._id}`, current);
            // setUploadVideoButtonText('Upload Video');
            //update ui
            if(data){
                let arr = subCategories.subCategory;
                const index = arr.findIndex((el) => el._id === current._id);
                arr[index] = current;
                setSubCategories({subCategory:[...arr]});
                
                setVisibleUpdateSubCategory(false);
                toast('Category updated');
            }
        } catch(err){
            toast('Category update failed');
        }
    }

    const handleDelete = async (id,index) => {
        const answer = window.confirm('Are you sure you want to delete?');
        if(!answer) return;

        let allSubCategories = subCategories.subCategory;
        const removed = allSubCategories.splice(index, 1);
        
        setSubCategories({subCategory:[...allSubCategories]});
    
        toast('Sub Category deleted');

        const {data} = await axios.delete(`/api/sub-category/${slug}/${id}`);
    }

    return(
        <AdminRoute>
            {loading && <SyncOutlined spin className='d-flex justify-content-center display-1 text-danger p-5' />}
            <h1 className="jumbotron text-center square">
                Categories
            </h1>
            <Button onClick={() => setVisibleAddSubCategory(true)} type="primary" className='float-right'>Add Sub Category</Button>
            <br/><br/>
            <List itemLayout="horizontal" dataSource={subCategories && subCategories.subCategory} renderItem={(item, index) => (
                <Item>
                    <Item.Meta 
                    avatar={<Avatar>{index + 1}</Avatar>} 
                    title={item.name}></Item.Meta>
                    <EditOutlined onClick={() => {setVisibleUpdateSubCategory(true); setCurrent(item); }} className='text-warning float-right pr-2' />
                    <DeleteOutlined onClick={() => {handleDelete(item._id, index)}} className='text-danger float-right' />
                </Item>
            )}>
            </List>

            <Modal title="Add Sub Category" centered visible={visibleAddSubCategory} onCancel={() => setVisibleAddSubCategory(false)} footer={null} >
                <CategoryForm values={values} setValues={setValues} handleSubmit={handleAddSubCategory} />
            </Modal>

            <Modal title="Update Sub Category" centered visible={visibleUpdateSubCategory} onCancel={() => setVisibleUpdateSubCategory(false)} footer={null} >
                <CategoryForm values={current} setValues={setCurrent} handleSubmit={handleUpdateSubCategory}  />
            </Modal>

        </AdminRoute>
    )
}

export default SubCategories;