import {useContext,useEffect, useState} from 'react';
import {Context} from '../../context';
import {useRouter} from 'next/router';
import AdminRoute from '../../components/routes/AdminRoute';
import axios from 'axios';
import {Avatar, List, Button, Modal} from 'antd';
import Link from 'next/link';
import {SyncOutlined, DeleteOutlined, EditOutlined} from '@ant-design/icons'
import CategoryForm from '../../components/forms/CategoryForm';
import {toast} from 'react-toastify';

const {Item} = List;

const Categories = () => {

    const router = useRouter();

    const {state: {user},} = useContext(Context);

    const [loading, setLoading] = useState(false);
    const [visibleAddCategory, setVisibleAddCategory] = useState(false);
    const [visibleUpdateCategory, setVisibleUpdateCategory] = useState(false);
    const [current, setCurrent] = useState({});
    const [categories, setCategories] = useState({category:[]});
    const [values, setValues] = useState({
        name:''
    });

    useEffect(() => {
        const fetchCategory = async () => {
            const {data} = await axios.get(`/api/category`);
            if(data) setCategories({category:[...data]});
        }
        fetchCategory();
    },[]);

    const handleAddCategory = async (e) => {
        e.preventDefault();
        // console.log(values);
        try{
            const {data} = await axios.post(`/api/category`, values);
            // console.log([...categories.category,data]);
            if(data) setCategories({category:[...categories.category,data]});
            setValues({name:''});
            setVisibleAddCategory(false);
            toast('Category added');
        } catch(err){
            console.log(err);
            toast('Category add failed');
        }
    }

    const handleUpdateCategory = async (e) => {
        e.preventDefault();
        // console.log(values);
        try{
            const {data} = await axios.put(`/api/category/${current._id}`, current);
            // setUploadVideoButtonText('Upload Video');
            
            //update ui
            if(data){
                let arr = categories.category;
                const index = arr.findIndex((el) => el._id === current._id);
                arr[index] = current;
                setCategories({category:[...arr]});
                
                setVisibleUpdateCategory(false);
                toast('Category updated');
            }
        } catch(err){
            toast('Category update failed');
        }
    }

    const handleDelete = async (id,index) => {
    //     console.log(id, index);
        const answer = window.confirm('Are you sure you want to delete?');
        if(!answer) return;

        let allCategories = categories.category;
        const removed = allCategories.splice(index, 1);
        
        setCategories({category:[...allCategories]});
    
        toast('Category deleted');

        const {data} = await axios.delete(`/api/category/${id}`);
        
            
    //     // //send request to server
    //     // const {data} = await axios.put(`/api/course/${slug}/${removed[0]._id}`);
    //     // console.log("LESSON DELETED => ",data);
    }

    return(
        <AdminRoute>
            {loading && <SyncOutlined spin className='d-flex justify-content-center display-1 text-danger p-5' />}
            <h1 className="jumbotron text-center square">
                Categories
            </h1>
            <Button onClick={() => setVisibleAddCategory(true)} type="primary" className='float-right'>Add Category</Button>
            <br/><br/>
            <List itemLayout="horizontal" dataSource={categories && categories.category} renderItem={(item, index) => (
                <Item>
                    <Item.Meta 
                    onClick={() => { router.push(`/admin/categories/${item._id}`);
                    }} 
                    avatar={<Avatar>{index + 1}</Avatar>} 
                    title={item.name}></Item.Meta>
                    <EditOutlined onClick={() => {setVisibleUpdateCategory(true); setCurrent(item); }} className='text-warning float-right pr-2' />
                    <DeleteOutlined onClick={() => {handleDelete(item._id, index)}} className='text-danger float-right' />
                </Item>
            )}>
            </List>

            <Modal title="Add Category" centered visible={visibleAddCategory} onCancel={() => setVisibleAddCategory(false)} footer={null} >
                <CategoryForm values={values} setValues={setValues} handleSubmit={handleAddCategory} />
            </Modal>

            <Modal title="Update Category" centered visible={visibleUpdateCategory} onCancel={() => setVisibleUpdateCategory(false)} footer={null} >
                <CategoryForm values={current} setValues={setCurrent} handleSubmit={handleUpdateCategory}  />
            </Modal>

        </AdminRoute>
    )
}

export default Categories;