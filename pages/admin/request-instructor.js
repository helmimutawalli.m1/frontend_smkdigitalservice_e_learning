import {useContext,useEffect, useState} from 'react';
import {Context} from '../../context';
import AdminRoute from '../../components/routes/AdminRoute';
import axios from 'axios';
import {Avatar, List, Tooltip} from 'antd';
import Link from 'next/link';
import {SyncOutlined, PlayCircleOutlined, CheckCircleOutlined, CloseCircleFilled} from '@ant-design/icons'
import { toast } from 'react-toastify';

const {Item} = List;

const RequestInstructor = () => {
    const {state: {user},} = useContext(Context);

    const [loading, setLoading] = useState(false);
    const [requestInstructors, setRequestInstructors] = useState({request: []});

    useEffect( () => {
        fetchRequestInstructor();
    },[]);

    const fetchRequestInstructor = async () => {
        try{
            const {data} = await axios.get(`/api/request-instructor`);
            if(data != null){
                setRequestInstructors({request:data});
            }
        }catch(err){
        }
    };

    const handleRemoveRequestInstructor = async (e,index, requestInstructor) => {
        e.preventDefault();
        try{
            let allRequestInstructor = requestInstructors.request;
            const removed = allRequestInstructor.splice(index, 1);
            setRequestInstructors({request:allRequestInstructor});
            const {data} = await axios.post(`/api/remove-request-instructor`,{requestInstructor});

            toast('Success Accept Request Instructor');
        }catch(err){
            toast('Failed Accept Request Instructor');

        }
    }

    const handleAcceptRequestInstructor = async (e,index, requestInstructor) => {
        e.preventDefault();
        try{
            let allRequestInstructor = requestInstructors.request;
            const removed = allRequestInstructor.splice(index, 1);
            setRequestInstructors({request:allRequestInstructor});
            const {data} = await axios.post(`/api/accept-request-instructor`,{requestInstructor});
            toast('Success Removed Request Instructor');
        }catch(err){
            toast('Failed Removed Request Instructor');
        }
    }

    return(
        <AdminRoute>
            {loading && <SyncOutlined spin className='d-flex justify-content-center display-1 text-danger p-5' />}
            <h1 className="jumbotron text-center square">
                Request Instructor
            </h1>
            <List itemLayout="horizontal" dataSource={requestInstructors && requestInstructors.request} renderItem={(item, index) => (
                <Item>
                    <Item.Meta avatar={<Avatar>{index + 1}</Avatar>} title={item.user && item.user.name} />
                    <Tooltip title='Decline'>
                        <span onClick={(e) => handleRemoveRequestInstructor(e,index, item)}  >
                            <CloseCircleFilled className='h5 pointer text-danger d-flex justify-content-center pr-2 pointer' />
                        </span>
                    </Tooltip>
                    <Tooltip title='Accept'>
                        <span onClick={(e) => handleAcceptRequestInstructor(e,index, item)} >
                            <CheckCircleOutlined className="h5 pointer text-success d-flex justify-content-center pointer" />
                        </span>
                    </Tooltip>
                </Item>
            )}>
            </List>
        </AdminRoute>
    )
}

export default RequestInstructor;