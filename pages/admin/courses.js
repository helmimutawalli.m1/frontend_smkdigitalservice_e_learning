import {useContext,useEffect, useState} from 'react';
import {Context} from '../../context';
import AdminRoute from '../../components/routes/AdminRoute';
import axios from 'axios';
import {Avatar, Tooltip} from 'antd';
import Link from 'next/link';
import {SyncOutlined, PlayCircleOutlined, CheckCircleOutlined, CloseCircleOutlined} from '@ant-design/icons'

const Courses = () => {
    const {state: {user},} = useContext(Context);

    const [loading, setLoading] = useState(false);

    const [courses, setCourses] = useState([]);

    useEffect(() =>{
        loadCourses();
    },[]);

    const loadCourses = async () => {
        const {data} = await axios.get(`/api/courses`);
        setCourses(data);
    }

    const myStyle = {marginTop: '-15px', fontSize: '10px'}

    return(
        <AdminRoute>
            {loading && <SyncOutlined spin className='d-flex justify-content-center display-1 text-danger p-5' />}
            <h1 className="jumbotron text-center square">
                Courses
            </h1>
            {courses && courses.map(course => (
                <>
                    <div className="media pt-2">
                        <Avatar size={50} src={course.image ? course.image.Location : '/course.png'} />

                        <div className="media-body pl-2">
                            <div className="row">
                                <div className="col">
                                    <Link href={`/instructor/course/view/${course.slug}`} className='pointer' >
                                        <a className="h5 mt-2 text-primary"><h5 className="pt-2">{course.name}</h5></a>
                                    </Link>
                                    <p style={{marginTop:"-10px"}}>{course.lessons.length} Lessons</p>
                                </div>
                                <div className="col-md-3 mt-5 text-center">
                                        {course.code == null ? "-" : course.code}
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ))}
        </AdminRoute>
    )
}

export default Courses;