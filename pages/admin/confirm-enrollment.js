import {useContext,useEffect, useState} from 'react';
import {Context} from '../../context';
import AdminRoute from '../../components/routes/AdminRoute';
import axios from 'axios';
import {Avatar, List, Tooltip} from 'antd';
import Link from 'next/link';
import {SyncOutlined, CheckCircleOutlined, CloseCircleFilled} from '@ant-design/icons'
import { toast } from 'react-toastify';

const {Item} = List;

const ConfirmEnrollment = () => {
    const {state: {user},} = useContext(Context);

    const [initLoading, setInitLoading] = useState(true);
    const [loading, setLoading] = useState(false);
    const [requestCourses, setRequestCourses] = useState({request: []});

    useEffect( () => {
        fetchRequestCourse();
        setInitLoading(false);
    },[]);

    const fetchRequestCourse = async () => {
        try{
            const {data} = await axios.get(`/api/request-course`);
            if(data != null){
                setRequestCourses({request:data});
            }
        }catch(err){
        }
    };

    const handleRemoveRequestCourse = async (e,index, requestCourse) => {
        e.preventDefault();
        try{
            let allRequestCourse = requestCourses.request;
            const removed = allRequestCourse.splice(index, 1);
            setRequestCourses({request:allRequestCourse});
            const {data} = await axios.post(`/api/remove-request-course`,{requestCourse});

            toast('Success Accept Request Course');
        }catch(err){
            toast('Failed Accept Request Course');

        }
    }

    const handleAcceptRequestCourse = async (e,index, requestCourse) => {
        e.preventDefault();
        try{
            let allRequestCourse = requestCourses.request;
            const removed = allRequestCourse.splice(index, 1);
            setRequestCourses({request:allRequestCourse});
            const {data} = await axios.post(`/api/accept-request-course`,{requestCourse});
            toast('Success Removed Request Course');
        }catch(err){
            toast('Failed Removed Request Course');
        }
    }


    return(
        <AdminRoute>
            {loading && <SyncOutlined spin className='d-flex justify-content-center display-1 text-danger p-5' />}
            <h1 className="jumbotron text-center square">
                Confirm Enrollment
            </h1>

            <List itemLayout="horizontal" dataSource={requestCourses && requestCourses.request} renderItem={(item, index) => (
                    <Item>
                        <Item.Meta avatar={<Avatar>{index + 1}</Avatar>} title={item.user && item.user.name} description={item.course && item.course.name} />
                        <div className='h5 justify-content-center pr-5' >{(item.course && item.course.code) ? item.course.code : "-" }</div>
                        <Tooltip title='Decline'>
                            <span onClick={(e) => handleRemoveRequestCourse(e,index, item)}  >
                                <CloseCircleFilled className='h5 pointer text-danger d-flex justify-content-center pr-2 pointer' />
                            </span>
                        </Tooltip>
                        <Tooltip title='Accept'>
                            <span onClick={(e) => handleAcceptRequestCourse(e,index, item)} >
                                <CheckCircleOutlined className="h5 pointer text-success d-flex justify-content-center pointer" />
                            </span>
                        </Tooltip>
                </Item>
            )}>
            </List>
        </AdminRoute>
    )
}

export default ConfirmEnrollment;