import {useContext,useEffect, useState} from 'react';
import {Context} from '../../context';
import AdminRoute from '../../components/routes/AdminRoute';
import axios from 'axios';
import {Avatar} from 'antd';
import Link from 'next/link';
import {SyncOutlined, PlayCircleOutlined} from '@ant-design/icons'

const AdminIndex = () => {
    const {state: {user},} = useContext(Context);

    const [loading, setLoading] = useState(false);

    return(
        <AdminRoute>
            {loading && <SyncOutlined spin className='d-flex justify-content-center display-1 text-danger p-5' />}
            <h1 className="jumbotron text-center square">
                Admin Dashboard
            </h1>
            
        </AdminRoute>
    )
}

export default AdminIndex;