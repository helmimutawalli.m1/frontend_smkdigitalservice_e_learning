import { useState, useContext } from "react";
import axios from "axios";
import { Carousel, Button, Card, Col, Row, Avatar} from 'antd';
import {Context} from '../context';
import Link from 'next/link';
import { BookOutlined, CalendarOutlined, LaptopOutlined, UserOutlined } from "@ant-design/icons";
const { Meta } = Card;

const Index = ({dataCourses}) => {
    const {state, dispatch} = useContext(Context);
    const {user} = state;

    function onChange(a, b, c) {
        console.log(a, b, c);
      }
      
      const contentStyle = {
        height: "500px",
        // color:"#fff",
        backgroundColor: "#fff",
        alignContent: "center"
      };


    return(
        <>
            <Carousel afterChange={onChange}>
                <div>
                    <div style={{height: "700px", backgroundColor: "#10203f"}}>
                        <div style={{paddingTop:"100px", paddingBottom:"100px"}}>
                            <div style={{justifyContent:"space-around",alignContent: "center", backgroundColor:"white", borderRadius:"10px", height:'450px'}} className="row ant-col-offset-2 col-md-10">
                                <div>
                                    <img src="/reading.png" alt="banner" className='img img-fluid' style={{height:'300px'}}  />
                                </div>
                                <div style={{marginTop: "auto", marginBottom: "auto"}}>
                                    <h1 className="mb-0" style={{fontWeight: 900}}>The Best</h1>
                                    <h1 style={{fontWeight: 900}}>e-Learning Courses</h1>
                                    <h3 style={{}}>For better World!</h3>
                                    <p style={{}}>Any subject, in any language, on any device, for all ages</p>
                                    {user ? 
                                    (<Link href="/courses">
                                        <Button type="primary" size="large" style={{borderRadius:"5px #10203f solid", backgroundColor: "#10203f"}} >GET STARTED</Button>
                                    </Link>)
                                    : (<Link href="/register">
                                        <Button type="primary" size="large" style={{borderRadius:"5px #10203f solid", backgroundColor: "#10203f"}} >Sign Up For Free</Button>
                                    </Link>)}
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <div>
                <h3 style={contentStyle}>2</h3>
                </div>
                <div>
                <h3 style={contentStyle}>3</h3>
                </div>
                <div>
                <h3 style={contentStyle}>4</h3>
                </div> */}
            </Carousel>
            <h1 className="text-center p-5">We are just, Out of Ordinary</h1>
            <div className="site-card-wrapper col-md-10 offset-1 pt-5 pb-5 ">
                <Row gutter={16}>
                <Col span={6}>
                    <Card
                        style={{ width: 300 }}
                        cover={
                            <LaptopOutlined style={{ fontSize: '300%', backgroundColor:"#7070f1", width: "30%", padding: "10px",margin: "25px", borderRadius: "10px"}} />
                        }
                    >
                        <Meta
                        title="Online Course"
                        description="This is the description"
                        />
                    </Card>
                </Col>
                <Col span={6}>
                    <Card
                        style={{ width: 300 }}
                        cover={
                            <UserOutlined style={{ fontSize: '300%', backgroundColor:"#eec2ae", width: "30%", padding: "10px",margin: "25px", borderRadius: "10px"}} />
                        }
                    >
                        <Meta
                        title="Expert Instructions"
                        description="This is the description"
                        />
                    </Card>
                </Col>
                <Col span={6}>
                    <Card
                        style={{ width: 300 }}
                        cover={
                            <BookOutlined style={{ fontSize: '300%', backgroundColor:"#aed3ee", width: "30%", padding: "10px",margin: "25px", borderRadius: "10px"}} />
                        }
                    >
                        <Meta
                        title="Effective Lessons"
                        description="This is the description"
                        />
                    </Card>
                </Col>
                <Col span={6}>
                    <Card
                        style={{ width: 300 }}
                        cover={
                            <CalendarOutlined style={{ fontSize: '300%', backgroundColor:"#e1eeae", width: "30%", padding: "10px",margin: "25px", borderRadius: "10px"}} />
                        }
                    >
                        <Meta
                        title="Course Schedule"
                        description="This is the description"
                        />
                    </Card>
                </Col>
                </Row>
            </div>

            <div>
                <div style={{height: "700px", backgroundColor: "#f9f9f9"}}>
                    <div style={{paddingTop:"100px", paddingBottom:"100px"}}>
                        <div style={{justifyContent:"space-around",alignContent: "center", backgroundColor:"#f9f9f9", borderRadius:"10px", height:'450px'}} className="row ant-col-offset-2 col-md-10">
                            <div style={{marginTop: "auto", marginBottom: "auto"}}>
                                <h1 className="mb-0" style={{fontWeight: 900}}>Level-up</h1>
                                <h1 style={{fontWeight: 900}}>your skills.</h1>
                                <p>Explore a groundbreaking new skillsets</p>
                            </div>
                            <div>
                                <img src="/reading.png" alt="banner" className='img img-fluid' style={{height:'300px'}}  />
                            </div>
                        </div>
                    </div>
                </div>
            </div>                    

            <h1 className="text-center p-5">Whats our students are saying</h1>
            <div className="site-card-wrapper col-md-10 offset-1 pt-5 pb-5 ">
                <Row gutter={16}>
                <Col span={8}>
                    <Card
                        style={{ width: 300 }}
                    >
                        <Meta
                            style={{padding: "10px"}}
                            avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                            title="Joe Komodo"
                            description="NTT"
                            />
                        <hr/>
                        <p>Study skills Student, cartoon lectures, child, reading, people png 1280x654px ... Study skills Learning Student Test, student, child, text, people png</p> 
                    </Card>
                </Col>
                <Col span={8}>
                    <Card
                        style={{ width: 300 }}
                        
                    >
                        <Meta
                            style={{padding: "10px"}}
                            avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                            title="Marci follower Mirana"
                            description="Tower2"
                            /> 
                            <hr/>
                        <p>Study skills Student, cartoon lectures, child, reading, people png 1280x654px ... Study skills Learning Student Test, student, child, text, people png</p> 
                    </Card>
                </Col>
                <Col span={8}>
                    <Card
                        style={{ width: 300 }}
                    >
                        <Meta
                            style={{padding: "10px"}}
                            avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                            title="Eldoge"
                            description="Forest"
                            /> 
                            <hr/>
                        <p>Study skills Student, cartoon lectures, child, reading, people png 1280x654px ... Study skills Learning Student Test, student, child, text, people png</p> 
                    </Card>
                </Col>
                </Row>
            </div>

            <div>
                <div style={{height: "500px"}}>
                    <div style={{paddingTop:"25px", paddingBottom:"25px"}}>
                        <div style={{justifyContent:"space-around",alignContent: "center", backgroundColor:"#f9f9f9", borderRadius:"10px", height:'400px'}} className="row ant-col-offset-2 col-md-10">
                            <div style={{marginTop: "auto", marginBottom: "auto"}}>
                                <h1 className="mb-0" style={{fontWeight: 900}}>Are you ready to start</h1>
                                <h1 style={{fontWeight: 900}}>your course now</h1>
                                <p>You can feel free to contact us for any need we are 24/7.</p>
                            </div>
                            <div style={{marginTop: "auto", marginBottom: "auto"}} >
                                <Link href="/courses"><Button type="primary" size="large" className="m-4" shape="round" style={{borderRadius:"5px #10203f solid", backgroundColor: "#10203f"}} >Lest Get Started</Button></Link>
                                <Button type="primary" size="large" className="m-4" shape="round" style={{borderRadius:"5px #10203f solid" }} ghost >Contact us</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </>
    )
}

export default Index;