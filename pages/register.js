import {useState, useEffect, useContext} from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { SyncOutlined } from '@ant-design/icons';
import Link from 'next/link';
import {Context} from '../context';
import { useRouter } from 'next/router';
import {GoogleLogin} from 'react-google-login';


const Register = () => {
    const [name,setName] = useState('');
    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');
    const [loading,setLoading] = useState(false);

    // const {state: {user}} = useContext(Context);
    const {state: {user}, dispatch} = useContext(Context);

    const router = useRouter();

    useEffect(() => {
        if(user !== null) router.push('/');
    },[user])

    const handleSubmit = async (e) => {
        e.preventDefault();
        // console.table({name,email, password});
        try{
            setLoading(true);
            const{data} = await axios.post(`/api/register`,{name,email,password});
            // console.log('register response', data);
            toast.success('Registration successful. Please login.');
            setName('');
            setEmail('');
            setPassword('');
            setLoading(false);
        }catch(err){
            toast.error(err.response.data);
            setLoading(false);
        }
    };

    const googleClientId = process.env.GOOGLE_CLIENT_ID;

    const handleGoogleRegister = async (googleData) => {
        try {
            setLoading(true);
            if(googleData.tokenId != undefined){
                const{data} = await axios.post(`/api/register/google`,{token: googleData.tokenId});
                toast.success('Registration successful.');
                dispatch({
                    type: "LOGIN",
                    payload: data,
                });
                window.localStorage.setItem('user', JSON.stringify(data));
                router.push('/user');
                setName('');
                setEmail('');
                setPassword('');
            }
            setLoading(false);
        } catch(err){
            toast.error(err.response.data);
            setLoading(false);
        }
    }

    return(
        <div className='register-container'>
            <div className='register-content col-md-4 offset-4 p-4'>
                <h1 className="login-heading-primary mb-2  pl-3">Register<span className="login-span-blue">.</span></h1>
                <p className="text-mute mb-2 pl-3">Enter your data to create your account.</p>
                
                <div className="container pb-4">
                    <form onSubmit={handleSubmit}>
                        <input type='text' className="form-control mb-4 p-4" value={name} onChange={e => setName(e.target.value)} placeholder="Enter name" required/>
                        <input type='email' className="form-control mb-4 p-4" value={email} onChange={e => setEmail(e.target.value)} placeholder="Enter email" required/>
                        <input type='password' className="form-control mb-4 p-4" value={password} onChange={e => setPassword(e.target.value)} placeholder="Enter password" required/>
                        <button type='submit' className='btn btn-block btn-primary col-md-12' disabled={!name || !email || !password || loading} >{loading ? <SyncOutlined spin /> : "Submit"}</button>
                    </form>
                    <p className='text-center p-3'>
                        Already registered?{" "}
                        <Link href='/login'><a>Login</a></Link>
                    </p>
                    <div className="line-breaker mb-2">
                        <span className="line"></span>
                        <span>or</span>
                        <span className="line"></span>
                    </div>
                    <GoogleLogin 
                        className='btn btn-block btn-primary col-md-12'
                        clientId='227763611443-c4gbpoci2s7smja3n1h4tdquvjl8onj8.apps.googleusercontent.com'
                        buttonText="Sign up with Google"
                        onSuccess={handleGoogleRegister}
                        onFailure={handleGoogleRegister}
                        cookiePolicy={'single_host_origin'}
                    />
                </div>
            </div>
        </div>
    )
}

export default Register;