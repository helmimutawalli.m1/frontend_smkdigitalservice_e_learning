import TopNav from "../components/TopNav";
import 'bootstrap/dist/css/bootstrap.min.css';
import "antd/dist/antd.css";
import "../public/css/styles.css";
import "../public/css/certificateStyles.css";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Provider} from '../context';
import Head from "next/head";

function MyApp ({Component, pageProps}) {
    return(
        <Provider>
            <Head>
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
                <title>E-Learning</title>
            </Head>
            <ToastContainer position='top-center' />
            <TopNav />
            <Component {...pageProps} />
        </Provider>
    )
}

export default MyApp;