import { useContext, useState, useEffect } from "react";
import { Context } from "../../context";
import { Button } from "antd";
import axios from "axios";
import { SettingOutlined, UserSwitchOutlined, LoadingOutlined, SyncOutlined } from "@ant-design/icons";
import { toast } from "react-toastify";
import UserRoute from "../../components/routes/UserRoute";

const BecomeInstructor = () => {
    //state
    const [ok, setOk] = useState(false);
    const [bank, setBank] = useState('');
    const [accountNumber, setAccountNumber] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [loading, setLoading] = useState(false);
    const {state: {user}} = useContext(Context);

    // const becomeInstructor = () => {
    //     // console.log('become instructor');
    //     setLoading(true);
    //     axios.post(`/api/make-instructor`).then(res => {
    //         console.log(res);
    //         window.location.href = res.data;
    //     }).catch(err => {
    //         console.log(err.response.status);
    //         toast.error('Stripe onboarding failed. Try again.');
    //         setLoading(false);
    //     })
    // }

    useEffect( () => {
        fetchRequestInstructorStatus();
    },[]);

    const fetchRequestInstructorStatus = async () => {
        try{
            const {data} = await axios.get(`/api/request-instructor-status`);
            // console.log(data);
            if(data.ok) setOk(true);
        }catch(err){

        }
        
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        setLoading(true);
        axios.post(`/api/make-instructor`,{bank, accountNumber, phoneNumber}).then(res => {
            console.log(res);
            toast.success('Request to be Instructor Success.');
            setLoading(false);
            setOk(true);
        }).catch(err => {
            console.log(err.response.status);
            toast.error('Request failed. Try again.');
            // toast.error('Stripe onboarding failed. Try again.');
            setLoading(false);
        })
    }

    return(
        <>
            <h1 className="jumbotron text-center square">Become Instructor</h1>
            <div className="container">
                <div className="row">
                    <div className="col-md-6 offset-md-3 text-center">
                        <div className="pt-4">
                            <UserSwitchOutlined className="display-1 pb-3" />
                            <br/>
                            {/* <h2>Setup payout to publish courses on SMK Digital Service</h2>
                            <p className="lead text-warning">SMK Digital Service partner with stripe to transfer earnings to your bank account</p>

                            <Button className="mb-3" type="primary" block shape="round" icon={loading ? <LoadingOutlined/> : <SettingOutlined/>}
                            size="large" onClick={becomeInstructor} disabled={user && user.role && user.role.includes('Instructor') || loading}>
                                {loading ? "Processing..." : "Payout Setup"}
                            </Button>

                            <p className="lead">You will be redirected to stripe to complete onboarding process</p> */}
                            {ok ? <>
                                <h2>Request Success</h2>
                                <p className="lead text-warning">waiting for admin to accept</p>
                            </> : <>
                                <h2>Setup payout to publish courses on SMK Digital Service</h2>
                                <form onSubmit={handleSubmit}>
                                    <input type='text' className="form-control mb-4 p-4" value={bank} onChange={e => setBank(e.target.value)} placeholder="Enter Bank Name" required/>
                                    <input type='number' className="form-control mb-4 p-4" value={accountNumber} onChange={e => setAccountNumber(e.target.value)} placeholder="Enter Account Number" required/>
                                    <input type='number' className="form-control mb-4 p-4" value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)} placeholder="Enter Phone Number" required/>
                                    <button type='submit' className='btn btn-block btn-primary col-md-12' disabled={!bank || !accountNumber || !phoneNumber || loading} >{loading ? <SyncOutlined spin /> : "Submit"}</button>
                                </form>
                            </>}
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default BecomeInstructor;