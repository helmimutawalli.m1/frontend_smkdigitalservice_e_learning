import React, {useState, useEffect, useContext, Component, useRef} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import {Context} from '../../../context';
import { Button } from 'antd';
// import { exportComponentAsPNG } from "react-component-export-image";
import {
    exportComponentAsPNG
  } from "../../../utils/export";
import { DownloadOutlined } from '@ant-design/icons';


const Certificate = () => {
    const {state, dispatch} = useContext(Context);
    const {user} = state;

    //router
    const router = useRouter();
    const {slug} = router.query;
    const [course, setCourse] = useState({lessons: []});
    const [completedLessons,setCompletedLessons] = useState([]);
    const [complete, setComplete] = useState(false);
    useEffect(() => {
        if(slug) loadCourse();
    },[slug]);

    useEffect(() => {
        if(course) loadCompletedLessons();
    },[course]);

    const loadCourse = async () => {
        const {data} = await axios.get(`/api/user/course/${slug}`);
        setCourse(data);
    };

    const loadCompletedLessons = async () => {
        const {data} = await axios.post(`/api/list-completed`, {courseId: course._id, });
        console.log('3')
        setCompletedLessons(data);
        if(data){
            (data.length == (course && course.lessons.length)) ? setComplete(true): setComplete(false)
        }
    }

    const componentRef = useRef();

        return ( 
            <>
                {!complete && <p className='h5 text-center'>Course Not completed</p>}
                {complete && 
                        <div className='row p-0 m-0'>
                            <div className='col-md-12'>
                                <div id="downloadWrapper" 
                                // ref={certificateWrapper}
                                ref={componentRef}
                                >
                                    <div id="certificateWrapper">
                                    <p className='intro col-md-12 text-center'>We are proudly present this to</p>
                                    <p className='name col-md-12 text-center'>{user && user.name}</p>
                                    <p className='description col-md-12 text-center'>We give this because {user && user.name} has finished the course {course && course.name}</p>
                                    <img src="/Certificate.png" alt="Certificate" />
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-12 text-center'>
                                <Button 
                                    onClick={(e) => {
                                        e.preventDefault();
                                        exportComponentAsPNG(componentRef, {
                                            fileName:`certificate-[${course && course.name}]-${user && user.name}`,
                                            html2CanvasOptions: { backgroundColor: null }
                                        });
                                        }} 
                                    type="primary" 
                                    shape="round" 
                                    icon={<DownloadOutlined />} 
                                    size={'large'}
                                >
                                    Download
                                </Button>
                            </div>
                        </div>
                }
            </>
      );    
  }

export default Certificate;