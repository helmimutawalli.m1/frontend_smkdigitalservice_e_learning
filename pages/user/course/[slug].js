import {userState, useEffect, useState, createElement, useContext} from 'react';
import {useRouter} from 'next/router';
import axios from 'axios';
import StudentRoute from '../../../components/routes/StudentRoute';
import {Button, Menu, Avatar, List} from 'antd';
import ReactPlayer from 'react-player';
import ReactMarkdown from 'react-markdown';
import { MenuFoldOutlined, MenuUnfoldOutlined, PlayCircleOutlined, CheckCircleFilled, MinusCircleFilled } from '@ant-design/icons';
import AddCommentForm from '../../../components/forms/AddCommentForm';
import CommentList from '../../../components/list/commentList';
import { toast } from "react-toastify";
import {Context} from '../../../context';
// const {Item} = List;
const {Item} = Menu;

const SingleCourse = () => {

    const {state, dispatch} = useContext(Context);
    const {user} = state;

    //state
    const [clicked, setClicked] = useState(-1);
    const [collapsed, setCollapsed] = useState(false);
    const [loading, setLoading] = useState(false);
    const [course, setCourse] = useState({lessons: []});
    const [ completedLessons,setCompletedLessons] = useState([]);
    //force state update
    const [updateState, setUpdateState] = useState(false);

    //state
    const [values, setValues] = useState({
        courseId:'',
        content:''
    });
    const [ discussion, setDiscussion] = useState({comments: []});
    const [ submitting, setSubmitting] = useState(false);
submitting

    //router
    const router = useRouter();
    const {slug} = router.query;

    useEffect(() => {
        if(slug) loadCourse();
    },[slug]);

    useEffect(() => {
        if(course){
            loadCompletedLessons();
            setValues({...values, courseId: course._id});
            loadDiscussion();
        } 
    },[course]);

    const loadCourse = async () => {
        const {data} = await axios.get(`/api/user/course/${slug}`);
        setCourse(data);
    };

    const loadCompletedLessons = async () => {
        const {data} = await axios.post(`/api/list-completed`, {courseId: course._id, });
        setCompletedLessons(data);
    }

    const loadDiscussion = async () => {
        if(course._id){
            const {data} = await axios.get(`/api/comment/${course._id}`);
            setDiscussion(data);
        }
    };

    const markCompleted = async () => {
        const {data} = await axios.post(`/api/mark-completed`,{
            courseId: course._id,
            lessonId: course.lessons[clicked]._id,
        });
        setCompletedLessons([...completedLessons, course.lessons[clicked]._id]);
    }

    const markInCompleted = async () => {
        try{
            const {data} = await axios.post(`/api/mark-incomplete`,{
                courseId: course._id,
                lessonId: course.lessons[clicked]._id,
            });
            const all = completedLessons;
            const index = all.indexOf(course.lessons[clicked]._id);
            if(index > -1){
                all.splice(index,1);
                setCompletedLessons(all);
                setUpdateState(!updateState);
            }
        }catch(err){
            console.log(err);
        }
    }

    const handleAddComment = async (e) => {
        e.preventDefault();
        try{
            setSubmitting(true);
            setValues({...values, content: ''});
            const {data} = await axios.post(`/api/comment`, {
                ...values,
            });
            setSubmitting(false);
            setDiscussion({comments: [...discussion.comments, data]})
        }catch(err){
            toast(err.response.data);
        }
        
    }

    return(
        <StudentRoute>
            <div className='row'>
                <div  style={{maxWidth:320}}>
                    <Button onClick={() => setCollapsed(!collapsed)} className='text-primary mt-1 btn-block mb-2' >{createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined  )}{!collapsed &&'Lessons'}</Button>
                    <Menu defaultSelectedKeys={[clicked]} inlineCollapsed={collapsed} style={{height:'86vh', overflowY:"scroll"}} >
                        {course.lessons.map((lesson, index) => (
                            <Item onClick={() => setClicked(index)} key={index} icon={<Avatar>{index + 1}</Avatar>} >
                                {lesson.title.substring(0,30)}{" "}{completedLessons.includes(lesson._id) ? (
                                    <CheckCircleFilled className='float-right text-primary ml-2' style={{marginTop:'13px'}}/>
                                ) : (
                                    <MinusCircleFilled className='float-right text-danger ml-2' style={{marginTop:'13px'}}/>
                                )}
                            </Item>
                        ))}
                    </Menu>
                </div>
                <div className='col'>
                    {clicked !== -1 ? (
                        <>

                            <div className='col alert alert-primary square'>
                                <b className='pl-4'>{course.name}</b>
                                {completedLessons.includes(course.lessons[clicked]._id) ? (
                                    <span className='float-right pointer' onClick={markInCompleted}>Mark as incomplete</span>
                                ):(
                                    <span className='float-right pointer' onClick={markCompleted}>Mark as completed</span>
                                )}
                            </div>

                            {course.lessons[clicked].video && course.lessons[clicked].video.Location && (
                                <>
                                    <div className='wrapper'>
                                        <ReactPlayer className='player' url={course.lessons[clicked].video.Location} width='100%' height='60vh' controls onEnded={() => markCompleted()} />

                                    </div>
                                </>
                            ) }
                            <div className='pt-5 pl-5 pr-5 '>
                                <h3>{course.lessons[clicked].title.substring(0,30)}</h3>
                                <ReactMarkdown children={course.lessons[clicked].content} className='single-post' />
                            </div>

                            
                        </>
                    ) : (
                        <div className='d-flex justify-content-center p-5' >
                            <div className='text-center p-5'>
                                <PlayCircleOutlined className='text-primary display-1 p-5' />
                                <p>Click on the lessons to start learning</p>
                            </div>
                        </div>
                    )}

                    {/* discussion */}
                    <h3 className='pt-5 pl-5 pr-5 '>Discussions</h3>
                    {discussion.comments.length > 0 && 
                        <div className='p-5' >
                            <CommentList discussion={discussion} />
                        </div>
                    }
                    <div className='pt-5 pl-5 pr-5 '>
                        <AddCommentForm 
                            values={values}
                            setValues={setValues}
                            handleAddComment={handleAddComment}
                            submitting={submitting}
                            avatar={user && user.picture}
                        />
                    </div>
                </div>
            </div>
        </StudentRoute>
    )
}

export default SingleCourse;