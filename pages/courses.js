import { useState, useEffect, useContext } from "react";
import axios from "axios";
import CourseCard from "../components/cards/CourseCard";
import CourseCard2 from "../components/cards/CourseCard2";
import { Input, Select, Carousel, Button, Empty   } from 'antd';
import {Context} from '../context';

const { Search } = Input;
const { Option } = Select;

const Index = ({dataCourses}) => {
    const [courses, setCourses] = useState(dataCourses ? dataCourses : []);
    const [categories, setCategories] = useState([]);
    const [search, setSearch] = useState('');
    const [category, setCategory] = useState('');
    const [subCategories, setSubCategories] = useState([]);
    const [subCategory, setSubCategory] = useState('');
    
    const {state, dispatch} = useContext(Context);
    const {user} = state;

    // useEffect(() => {
    //     const fetchCourses = async () => {
    //         const {data} = await axios.get(`/api/courses`);
    //         setCourses(data);
    //     }
    //     fetchCourses();
    // },[]);

    
    const onSearch = (value) => {
        // console.log("SEARCH",{courseName:value,category:category, subCategory: subCategory});
        setSearch(value);
        loadCourses({courseName:value,category:category, subCategory: subCategory, limit:{start:1,end:100}, order: {orderBy:"updatedAt",isDescending:false}});
    };
    
    const loadCourses = async (payload) => {
        // console.log("PAYLOAD ==> ", payload)
        const {data} = await axios.post(`/api/courses`,payload);
        if(data) setCourses(data);
    }

    useEffect(() => {
        const fetchCategory = async () => {
            const {data} = await axios.get(`/api/category`);
            if(data) setCategories(data);
        }
        const fetchSubCategory = async () => {
            const {data} = await axios.get(`/api/sub-category`);
            if(data) setSubCategories(data);
        }
        fetchCategory();
        fetchSubCategory();
    },[]);

    const handleSearchChange = value => {
        setSearch(value);
    };
    const handleCategoryChange = async (value) => {
        setCategory(value);
        loadCourses({courseName:search,category:value, subCategory: subCategory, limit:{start:1,end:100}, order: {orderBy:"updatedAt",isDescending:false}})
    };

    const handleSubCategoryChange = async (value) => {
        setSubCategory(value);
        loadCourses({courseName:search,category:category, subCategory: value, limit:{start:1,end:100}, order: {orderBy:"updatedAt",isDescending:false}})
    };

    function onChange(a, b, c) {
        console.log(a, b, c);
      }
      
      const contentStyle = {
        height: "500px",
        backgroundColor: "#10203f",
        justifyContent:"space-around",
        alignContent: "center"
      };


    return(
        <>
            {/* <h1 className="jumbotron text-center bg-primary square">SMK Digital Service E-Learning</h1> */}
            <div className="text-center p-5">
                <h1 className="mb-0">Choose and Study Any Class</h1>
                <h1>what you need</h1>
            </div>
            <div className="row m-0 pt-4 pl-0 pr-0">
                    <div className="col-md-10 offset-1">
                        <div className="col-md-4 p-0 offset-4">
                            <Search className="container pb-2 pl-4 pr-4"  placeholder="search courses..." onChange={handleSearchChange} onSearch={onSearch} /> 
                        </div>
                    </div>
                    <div className="col-md-10 offset-1 row">
                        <div className="col-md-4 p-0 offset-4">
                            <Input.Group compact className="pr-4 pl-4">
                               <Select className="pr-2" defaultValue={''} style={{width:'50%'}} onChange={handleCategoryChange} >
                                <Option key={''}>Categories</Option>
                                {categories.map(category => (
                                <Option key={category.name}>{category.name}</Option>
                                ))}
                            </Select>
                            <Select defaultValue={''} style={{width:'50%'}} onChange={handleSubCategoryChange}>
                                <Option key={''}>Sub Categories</Option>
                                {subCategories.map(subCategory => (
                                <Option key={subCategory.name}>{subCategory.name}</Option>
                                ))}
                            </Select>
                            </Input.Group>
                        </div>
                    </div>
            </div>
            
            <div className="container-fluid pt-5 pl-0 pr-0">
                    <div className="col-md-10 offset-1 row">
                        {courses.length > 0 ? courses.map((course) => <div key={course._id} className='col-md-4 p-4'>{
                            // <pre>{JSON.stringify(course,null,4)}</pre>
                            <CourseCard2 className='dashboard-card' course={course} />
                        }</div>): 
                        (<h3 className="col-md-12 text-center"><Empty /></h3>)}
                    </div>
            </div>
        </>
    )
}

export async function getServerSideProps(){
    const {data} = await axios.post(`${process.env.API}/api/courses`, {courseName:'',category:'', subCategory: '', limit:{start:1,end:100}, order: {orderBy:"updatedAt",isDescending:false}});
    return {
        props:{
            dataCourses: data
        },
    };
}

export default Index;