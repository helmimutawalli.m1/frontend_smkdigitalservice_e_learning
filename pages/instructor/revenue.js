import { useState, useEffect, useContext } from "react";
import {Context} from '../../context';
import InstructorRoute from "../../components/routes/InstructorRoute";
import axios from "axios";
import { DollarOutlined, SettingOutlined, LoadingOutlined, SyncOutlined } from "@ant-design/icons";
import {stripeCurrencyFormatter} from '../../utils/helpers';
import { currencyFormatter } from '../../utils/helpers';

const InstructorRevenue = () => {

    const [balance, setBalance] = useState({pending:[]});
    const [revenue, setRevenue] = useState(0);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        // sendBalanceRequest();
        sendRevenueRequest();
    },[]);

    const sendBalanceRequest = async () => {
        const {data} = await axios.get(`/api/instructor/balance`);
        setBalance(data);
    }

    const sendRevenueRequest = async () => {
        const {data} = await axios.post(`/api/instructor/revenue`);
        console.log(data);
        let totalRevenue = data.reduce(function (accumulator, item) {
            return accumulator + item.price;
          }, 0);
        // var test = findOcc(data,"course");
        // console.log(test);

        setRevenue(totalRevenue);
    }

    // const findOcc = (arr, key) => {
    //     let arr2 = [];
          
    //     arr.forEach((x)=>{
             
    //       // Checking if there is any object in arr2
    //       // which contains the key value
    //        if(arr2.some((val)=>{ return val[key] == x[key] })){
               
    //          // If yes! then increase the occurrence by 1
    //          arr2.forEach((k)=>{
    //            if(k[key] === x[key]){ 
    //              k["occurrence"]++
    //            }
    //         })
               
    //        }else{
    //          // If not! Then create a new object initialize 
    //          // it with the present iteration key's value and 
    //          // set the occurrence to 1
    //          let a = {}
    //          a[key] = x[key]
    //          a["occurrence"] = 1
    //          arr2.push(a);
    //        }
    //     })
          
    //     return arr2
    //   }

    const handlePayoutSettings = async () => {
        try{
            setLoading(true);
            const {data} = await axios.get(`/api/instructor/payout-settings`);
            window.location.href = data;
            setLoading(false);
        }catch(err){
            console.log(err);
            setLoading(false);
            alert('Unable to access payout settings. Try later.')
        }
    }

    return(
        <InstructorRoute>
            <div className="container">
                <div className="row pt-2">
                    <div className="col-md-8 offset-md-2 bg-light p-4">
                        <h2>Revenue Report <DollarOutlined className="float-right" />{" "}</h2>
                        {/* <small>You get paid directly from stripe to your bank account every 48 hour</small> */}
                        <hr/>
                        {/* {JSON.stringify(balance,null,4)} */}
                        {/* {JSON.stringify(balance.pending,null,4)} */}
                        <h4>Total revenue 
                            <span className='float-right'>{currencyFormatter({amount:revenue, currency:'idr'}).replace('IDR','Rp.')}</span>
                            {/* {balance.pending && balance.pending.map((bp,i) => (
                                <span key={i} className='float-right'>{stripeCurrencyFormatter(bp)}</span>
                            ))} */}
                        </h4>
                        <small>from all course enroll
                            {/* <span key={i} className='float-right'>{stripeCurrencyFormatter(bp)}</span> */}
                        </small>
                        {/* <hr/>
                        <h4>
                            Payouts{" "}
                            {!loading ? 
                                <SettingOutlined className="float-right" onClick={handlePayoutSettings} /> :
                                <SyncOutlined className="float-right pointer" spin />
                            } 
                        </h4>
                        <small>Update your stripe account details or view previous payouts.</small> */}
                        {/* <hr/>
                        <h4>Pending balance 
                            {balance.pending && balance.pending.map((bp,i) => (
                                <span key={i} className='float-right'>{stripeCurrencyFormatter(bp)}</span>
                            ))}
                        </h4> */}
                        {/* <small>For last 48 hours</small> */}
                    </div>
                </div>
            </div>
        </InstructorRoute>
    )
}

export default InstructorRevenue;