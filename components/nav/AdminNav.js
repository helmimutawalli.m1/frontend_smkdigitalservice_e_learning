import { useState, useEffect } from 'react';
import Link from 'next/link';
import { Menu } from 'antd';
import { BookOutlined, DollarCircleOutlined, LaptopOutlined, TagOutlined, UsergroupAddOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;

const AdminNav = () => {
    const [current, setCurrent] = useState('')

    useEffect(() => {
        process.browser && setCurrent(window.location.pathname);
    },[process.browser && window.location.pathname])

    return(

        <Menu
            mode="inline"
            defaultSelectedKeys={[`${process.browser && window.location.pathname}`]}
            defaultOpenKeys={[`${process.browser && window.location.pathname}`]}
            style={{ height: '100%', borderRight: 0 }}
        >
                <Menu.Item key="/admin" icon={<LaptopOutlined />}>
                    <Link href='/admin'>
                        <a className={`nav-link ${current === '/admin' && 'active'}`}>Dashboard</a>
                    </Link>
                </Menu.Item>
                <Menu.Item key="/admin/request-instructor" icon={<UsergroupAddOutlined />}>
                    <Link href='/admin/request-instructor'>
                        <a className={`nav-link ${current === '/admin/request-instructor' && 'active'}`}>Request Instructor</a>
                    </Link>
                </Menu.Item>
                <Menu.Item key="/admin/confirm-enrollment" icon={<DollarCircleOutlined />}>
                    <Link href='/admin/confirm-enrollment'>
                        <a className={`nav-link ${current === '/admin/confirm-enrollment' && 'active'}`}>Confirm Enrollment</a>
                    </Link>
                </Menu.Item>
                <Menu.Item key="/admin/courses" icon={<BookOutlined />}>
                    <Link href='/admin/courses'>
                        <a className={`nav-link ${current === '/admin/courses' && 'active'}`}>Courses</a>
                    </Link>
                </Menu.Item>
                <SubMenu key="sub1" icon={<TagOutlined />} title="Tag">
                        <Menu.Item key="/admin/categories">
                            <Link href='/admin/categories'>
                                <a className={`nav-link ${current === '/admin/categories' && 'active'}`}>Categories</a>
                            </Link>
                        </Menu.Item>
                </SubMenu>
                
        </Menu>

        // <div className='nav flex-column nav-pills'>
        //     <Link href='/admin'>
        //         <a className={`nav-link ${current === '/admin' && 'active'}`}>Dashboard</a>
        //     </Link>
        //     <Link href='/admin/request-instructor'>
        //         <a className={`nav-link ${current === '/admin/request-instructor' && 'active'}`}>Request Instructor</a>
        //     </Link>

        //     <Link href='/admin/confirm-enrollment'>
        //         <a className={`nav-link ${current === '/admin/confirm-enrollment' && 'active'}`}>Confirm Enrollment</a>
        //     </Link>

        //     <Link href='/admin/courses'>
        //         <a className={`nav-link ${current === '/admin/courses' && 'active'}`}>Courses</a>
        //     </Link>
        // </div>
    )
}

export default AdminNav;