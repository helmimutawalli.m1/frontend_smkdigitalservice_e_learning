import { useState, useEffect } from 'react';
import Link from 'next/link';
import { Menu } from 'antd';
import { LaptopOutlined } from '@ant-design/icons';

const UserNav = () => {
    const [current, setCurrent] = useState('');

    useEffect(() => {
        process.browser && setCurrent(window.location.pathname);
    },[process.browser && window.location.pathname])

    return(
        <Menu
            mode="inline"
            defaultSelectedKeys={[`${process.browser && window.location.pathname}`]}
            defaultOpenKeys={[`${process.browser && window.location.pathname}`]}
            style={{ height: '100%', borderRight: 0 }}
        >
                <Menu.Item key="/user" icon={<LaptopOutlined />}>
                    <Link href='/user'>
                        <a className={`nav-link ${current === '/user' && 'active'}`}>Dashboard</a>
                    </Link>
                </Menu.Item>
        </Menu>
        // <div className='nav flex-column nav-pills'>
        //     <Link href='/user'>
        //         <a className={`nav-link ${current === '/user' && 'active'}`}>Dashboard</a>
        //     </Link>
        // </div>
    )
}

export default UserNav;