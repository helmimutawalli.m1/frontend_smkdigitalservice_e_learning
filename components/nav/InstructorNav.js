import { useState, useEffect } from 'react';
import Link from 'next/link';
import { Menu } from 'antd';
import { BookOutlined, DollarCircleOutlined, LaptopOutlined } from '@ant-design/icons';

const InstructorNav = () => {
    const [current, setCurrent] = useState('')

    useEffect(() => {
        process.browser && setCurrent(window.location.pathname);
    },[process.browser && window.location.pathname])

    return(
        <Menu
            mode="inline"
            defaultSelectedKeys={[`${process.browser && window.location.pathname}`]}
            defaultOpenKeys={[`${process.browser && window.location.pathname}`]}
            style={{ height: '100%', borderRight: 0 }}
        >
                <Menu.Item key="/instructor" icon={<LaptopOutlined />}>
                    <Link href='/instructor'>
                        <a className={`nav-link ${current === '/instructor' && 'active'}`}>Dashboard</a>
                    </Link>
                </Menu.Item>
                <Menu.Item key="/instructor/course/create" icon={<BookOutlined />}>
                    <Link href='/instructor/course/create'>
                        <a className={`nav-link ${current === '/instructor/course/create' && 'active'}`}>Course Create</a>
                    </Link>
                </Menu.Item>
                <Menu.Item key="/instructor/revenue" icon={<DollarCircleOutlined />}>
                    <Link href='/instructor/revenue'>
                        <a className={`nav-link ${current === '/instructor/revenue' && 'active'}`}>Revenue</a>
                    </Link>
                </Menu.Item>
        </Menu>
        // <div className='nav flex-column nav-pills'>
        //     <Link href='/instructor'>
        //         <a className={`nav-link ${current === '/instructor' && 'active'}`}>Dashboard</a>
        //     </Link>
        //     <Link href='/instructor/course/create'>
        //         <a className={`nav-link ${current === '/instructor/course/create' && 'active'}`}>Course Create</a>
        //     </Link>

        //     <Link href='/instructor/revenue'>
        //         <a className={`nav-link ${current === '/instructor/revenue' && 'active'}`}>Revenue</a>
        //     </Link>
        // </div>
    )
}

export default InstructorNav;