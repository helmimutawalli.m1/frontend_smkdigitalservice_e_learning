import {useEffect, useState} from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { SyncOutlined } from '@ant-design/icons';
import InstructorNav from '../nav/InstructorNav';
import { Layout, Breadcrumb } from 'antd';

const { Content, Sider } = Layout;

const InstructorRoute = ({children}) => {
    //state
    const [ok, setOk] = useState(false);

    //router
    const router =useRouter();

    useEffect(() => {
        fetchInstructor();
    },[])

    const fetchInstructor = async () => {
        try{
            const {data} = await axios.get(`/api/current-instructor`);
            // console.log(data);
            if(data.ok) setOk(true)
        }catch(err){
            console.log(err);
            setOk(false);
            router.push('/')
        }
    }

    return(
        <>
            {!ok ? 
            (<SyncOutlined spin className='d-flex justify-content-center display-1 text-primary p-5' />) : 
            (
            
                <Layout style={{ minHeight: '90vh' }}>
                    <Sider width={200} className="site-layout-background">
                        <InstructorNav/>
                    </Sider>
                    <Layout style={{ padding: '0 24px 24px' }}>
                        <Breadcrumb style={{ margin: '16px 0' }}>
                            <Breadcrumb.Item>Home</Breadcrumb.Item>
                        </Breadcrumb>
                        <Content className="site-layout-background" style={{ padding: 24, margin: 0, minHeight: 280, backgroundColor:'#fff'}}>
                            {children}
                        </Content>
                    </Layout>
                </Layout>

            // <div className='container-fluid'>
            //     <div className='row'>
            //         <div className='col-md-2'>
            //             <InstructorNav/>
            //         </div>
            //         <div className='col-md-10'>
            //             {children}
            //         </div>
            //     </div>
            // </div>
            )
            }
        </>
    )
}

export default InstructorRoute;