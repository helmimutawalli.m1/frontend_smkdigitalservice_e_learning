import {useState, useEffect, useContext} from 'react'
import {Menu, Layout} from 'antd';
import Link from 'next/link';
import {AppstoreAddOutlined, CoffeeOutlined, LoginOutlined, LogoutOutlined, UserAddOutlined, CarryOutOutlined, TeamOutlined} from '@ant-design/icons';
import {Context} from '../context';
import axios from 'axios';
import {useRouter} from 'next/router';
import {toast} from 'react-toastify';

import { SEKOLAHSETTING } from '../data';

const {SubMenu, ItemGroup} = Menu;
const { Header } = Layout;
const TopNav = () => {
    const [current, setCurrent] = useState('');

    const {state, dispatch} = useContext(Context);
    const {user} = state;
    const router = useRouter();

    useEffect(() => {
        process.browser && setCurrent(window.location.pathname) 
    },[process.browser && window.location.pathname])

    const logout = async () => {
        dispatch({type:"LOGOUT"});
        window.localStorage.removeItem('user');
        const {data} = await axios.get(`/api/logout`);
        toast(data.message);
        router.push('/login');
    }

    return(
        <Header className="header p-0" >
                <Menu theme='light' mode='horizontal' selectedKeys={[current]} className='mb-2'>
                    <Menu.Item key='/' onClick={e => setCurrent(e.key)} icon={<img src={SEKOLAHSETTING.Logo} alt={SEKOLAHSETTING.Logo} className='img img-fluid' style={{height:'50px'}}  />}>
                        <Link href="/">
                            <a>{SEKOLAHSETTING.Nama}</a>
                        </Link>
                    </Menu.Item>
                    
                    {user && user.role && user.role.includes('Instructor') ? (
                        <Menu.Item key='/instructor/course/create' onClick={e => setCurrent(e.key)} icon={<CarryOutOutlined />}>
                            <Link href="/instructor/course/create">
                                <a>Create Course</a>
                            </Link>
                        </Menu.Item>
                    ) : user && user.role && user.role.includes('Subscriber') && !user.role.includes('Admin') ? (
                        <Menu.Item key='/user/become-instructor' onClick={e => setCurrent(e.key)} icon={<TeamOutlined/>} className='float-right'>
                            <Link href="/user/become-instructor">
                                <a>Become Instructor</a>
                            </Link>
                        </Menu.Item>
                    ) : (<></>) }

                    {user === null && 
                        (<>
                            <Menu.Item key='/login' onClick={e => setCurrent(e.key)} icon={<LoginOutlined/>} style={{ marginLeft: 'auto' }}>
                                <Link href="/login">
                                    <a>Login</a>
                                </Link>
                            </Menu.Item>
                            <Menu.Item key='/register' onClick={e => setCurrent(e.key)} icon={<UserAddOutlined/>}>
                                <Link href="/register">
                                    <a>Register</a>
                                </Link>
                            </Menu.Item>
                        </>
                        )
                    }

                    {user && user.role && user.role.includes('Instructor') && (
                        <Menu.Item key='/instructor' onClick={e => setCurrent(e.key)} icon={<TeamOutlined/>} style={{ marginLeft: 'auto' }} >
                            <Link href="/instructor">
                                <a>Instructor</a>
                            </Link>
                        </Menu.Item>
                    )}

                    
                    {user !== null && 
                        (
                            <SubMenu key="SubMenu" icon={<CoffeeOutlined />} title={user && user.name} style={user && user.role && !user.role.includes('Instructor') && { marginLeft: 'auto' }} >
                                
                                <ItemGroup>
                                    <Menu.Item key={user && user.role && user.role.includes('Admin') ?  '/admin' : '/user'}>
                                        <Link href={user && user.role && user.role.includes('Admin') ?  '/admin' : '/user'}><a>Dashboard</a></Link>
                                    </Menu.Item>
                                    <Menu.Item onClick={logout}>Logout</Menu.Item>
                                </ItemGroup>
                            </SubMenu>
                        )
                    }
                </Menu>
        </Header>
    )
}

export default TopNav;