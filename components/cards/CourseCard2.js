import { AppstoreOutlined, BookOutlined, BuildOutlined } from '@ant-design/icons';
import Link from 'next/link';
import { currencyFormatter } from '../../utils/helpers';


const CourseCard2 = ({course}) => {
    const {name,instructor,price, image,slug,paid,category} = course;
    return(
        <Link href={`/course/${slug}`} className="mb-4">
            <a>
               <div className='p-4 bg-white bg-slate-800 shadow-lg shadow-slate-700 flex flex-col ease-linear duration-300 flex-row-reverse' style={{height:"auto", borderRadius:10}}>
                    <div style={{height:"auto", width:"auto", borderRadius:"10px", position:"relative"}} className="shadow-md">
                        <div style={{zIndex:10, background:"#5865F2", position:"absolute", borderRadius:"10px 0 10px 0", font:"bold"}} className="pl-4 pr-4 pb-2 pt-2">
                            <h5 className='mb-0' style={{color:"white"}}>{ paid ? currencyFormatter({amount:price, currency:'idr'}).replace('IDR','Rp.') : 'Gratis'}</h5>
                        </div>
                        <div style={{height:"auto", width:"auto", position:"relative", border:"2px white solid", borderRadius:"10px" }} >
                            <img src={image.Location} alt="thumbnail" style={{width:'-webkit-fill-available', minHeight:200,maxHeight:200, layout:"fill", objectFit: 'cover', borderRadius:"10px" }}  className='' />
                        </div>
                    </div>  

                    <div style={{height:"auto", width:"auto"}}>
                        <p style={{color:"#5865F2", fontWeight:"bold"}} className="m-2 pl-1 text-lg">by {instructor.name}</p>
                        <h1 style={{fontWeight:"bold", fontSize:"1.5rem", height:"4rem"}} className="m-2 mb-5 titleCard">
                            {name}.
                        </h1>

                        <div style={{color:"black"}} className="pt-16 pr-2 pl-2 flex flex-row justify-around flex-wrap row">
                            {category && category.category && (
                                <div className="flex flex-row items-center m-2 row">
                                    <AppstoreOutlined size={20} />
                                    <p style={{marginTop:-5}} className="pl-1 mb-0">
                                        {category.category.name}
                                    </p>
                                </div>
                            )}
                            {category && category.subCategory && (
                                <div className="flex flex-row items-center m-2 row">
                                    <BuildOutlined size={20} />
                                    <p style={{marginTop:-5}} className="pl-1 mb-0">
                                        {category.subCategory.name}
                                    </p>
                                </div>
                            )}
                            <div className="flex flex-row items-center m-2 row">
                                <BookOutlined size={20} />
                                <p style={{marginTop:-5}} className="pl-1 mb-0">{course.lessons.length} lesson</p>
                            </div>
                        </div>
                    </div>

                    
                    
               </div>
            </a>
        </Link>
    )
}

export default CourseCard2;