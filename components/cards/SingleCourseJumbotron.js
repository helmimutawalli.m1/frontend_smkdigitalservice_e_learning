import {Badge, Modal, Button} from 'antd';
import { currencyFormatter } from '../../utils/helpers';
import ReactPlayer from 'react-player';
import {LoadingOutlined, SafetyOutlined} from '@ant-design/icons';

const SingleCourseJumbotron = ({course, showModal, setShowModal, preview, setPreview, loading, user, handleFreeEnrollment, handlePaidEnrollment, enrolled, setEnrolled}) => {

    //destructure
    const {name,description,instructor, updatedAt, lessons, image, price, paid, category} = course;


    return(
        <div className='jumbotron square' >
            <div className='col-md-10 offset-1'>
                <div className='row'>
                    <div className='col-md-8'>
                        {/* title */}
                        <h1 className='text-light font-weight-bold'>{name}</h1>
                        {/* description */}
                        <p className='lead'>{description && description.substring(0, 160)}...</p>
                        {/* category */}
                        <Badge count={category && category.category && category.category.name} style={{backgroundColor:'#03a9f4'}} className='pb-2 mr-2' />
                        <Badge count={category && category.subCategory && category.subCategory.name} style={{backgroundColor:'#03a9f4'}} className='pb-2 mr-2' />
                        <div className='row p-2'>
                            {/* author */}
                            <p className='p-2 mb-0'>Created by {instructor.name}</p>
                            {/* updated at */}
                            <p className='p-2 mb-0'>Last updated {new Date(updatedAt).toLocaleDateString()}</p>
                        </div>
                        {/* price */}
                        <h4 className='text-light'>{paid ? currencyFormatter({amount:price, currency:'idr'}).replace('IDR','Rp.') : 'Gratis' }</h4>
                    </div>
                    <div className='col-md-4 text-center'>
                        {/* show video preview or course image */}
                        {lessons[0].video && lessons[0].video.Location ? (
                                <div onClick={() => {
                                    setPreview(lessons[0].video.Location);
                                    setShowModal(!showModal);
                                }}>
                                    <ReactPlayer className='react-player-div' url={lessons[0].video.Location} width='100%' height='225px' light={image.Location} />
                                </div>
                            ) : (
                                <>
                                    <img src={image.Location} alt={name} className='img img-fluid' style={{height:'225px', borderRadius:10}} />
                                </>
                            )
                        }
                        {/* enroll button */}
                        {loading ? (
                            <div className='d-flex justify-content-center'><LoadingOutlined className='h1 text-center' /></div>
                        ) : (
                            <Button className='mb-3 mt-3' type='danger' block shape='round' icon={<SafetyOutlined />} size='large' disabled={loading || ( user && enrolled && enrolled.note === "Pending" )} onClick={paid ? handlePaidEnrollment : handleFreeEnrollment}>
                                {user ? (enrolled.status ? "Go to course" : ( enrolled.note === "Pending" ? "Wait Approve Admin" : "Enroll")) : "Login to enroll"}
                            </Button>
                        )}
                    </div>
                </div> 
            </div>
        </div>  
    )
}

export default SingleCourseJumbotron;