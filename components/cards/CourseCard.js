import {Card, Badge} from 'antd';
import Link from 'next/link';
import { currencyFormatter } from '../../utils/helpers';

const {Meta} = Card;

const CourseCard = ({course}) => {
    const {name,instructor,price, image,slug,paid,category} = course;
    return(
        <Link href={`/course/${slug}`} >
            <a>
                <Card className='mb-4' hoverable={true}  cover={<img src={image.Location} alt={name}  style={{height: '200px', objectFit: 'cover'}}  className='p-0 m-0' />}>
                    <Meta
                        title={name}
                        description={`by ${instructor.name}`}
                    />
                    {/* <h4 className='font-font-weight-bold'>{name}</h4> */}
                    {/* <p>by {instructor.name}</p> */}
                    <Badge count={category && category.category && category.category.name} style={{backgroundColor: '#03a9f4'}} className='pb-2 mr-2 mt-2' />
                    <Badge count={category && category.subCategory && category.subCategory.name} style={{backgroundColor: '#03a9f4'}} className='pb-2 mr-2 mt-2' />
                    <h5 className='pt-2'>{ paid ? currencyFormatter({amount:price, currency:'idr'}).replace('IDR','Rp.') : 'Gratis'}</h5>
                </Card>
            </a>
        </Link>
    )
}

export default CourseCard;