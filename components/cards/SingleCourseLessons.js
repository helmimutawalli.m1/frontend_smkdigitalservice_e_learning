import {List, Avatar} from 'antd';
const {Item} = List;

const SingleCourseLessons = ({lessons, setPreview, showModal, setShowModal}) => {
    return(
        <div className="col-md-10 offset-1">
            <div className="col lesson-list">
                {lessons && <h4>{lessons.length} Lessons</h4>}
                <hr/>
                <List itemLayout='horizontal' dataSource={lessons} renderItem={(item,index) => (
                        <Item key={index}>
                            <Item.Meta avatar={<Avatar>{ index + 1 }</Avatar>} title={item.title}/>
                                {item.video && item.video !== null && item.free_preview && (
                                    <span className='text-primary pointer' onClick={() => {
                                        setPreview(item.video.Location); 
                                        setShowModal(!showModal);
                                    }}>Preview</span>
                                )}
                        </Item>
                ) } />
            </div>
        </div>
    )
}

export default SingleCourseLessons;