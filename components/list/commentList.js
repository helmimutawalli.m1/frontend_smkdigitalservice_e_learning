import {List, Avatar, Comment, Tooltip, Image} from 'antd';
const {Item} = List;
import moment from 'moment';

const CommentList = ({discussion}) => {
    return(
        <div>
            <List
                dataSource={discussion.comments}
                header={`${discussion.comments.length} ${discussion.comments.length > 1 ? 'replies' : 'reply'}`}
                itemLayout="horizontal"
                renderItem={comment => <Comment
                    author={comment.user ? comment.user.name : "An"}
                    avatar={
                        (<Avatar src={<Image src={comment.user ? comment.user.picture : '/avatar.png'}  style={{ width: 32 }} />} />)
                        // comment.user ? comment.user.picture : '/avatar.png'
                    }
                    content={comment.content}
                    datetime={(
                        <Tooltip title={(new Date(comment.createdAt)).toISOString().slice(0, 19).replace(/-/g, "-").replace("T", " ")}>
                            <span>{(new Date(comment.createdAt)).toISOString().slice(0, 19).replace(/-/g, "-").replace("T", " ")}</span>
                        </Tooltip>
                    )}
                />}
            />
        </div>
        // <div className='col-md-12'>
        //     {discussion && discussion.comments && discussion.comments.map((comment) => (
        //         <div key={comment._id} className='media p-0 m-0'>
        //             {/* <div className='col-md-2'>
        //                 <Avatar size={40} shape='square' src={comment.user ? comment.user.picture : '/avatar.png'} />
        //             </div> */}
        //             <div className='col-md-12 media-body pl-2'>
        //                 <div className='row'>
        //                     <div className='col-md-8'>
        //                         <h5 className='mt-0 '>{comment.user ? comment.user.name : "An"}</h5>
        //                     </div>
        //                     <div className='col-md-4'>
        //                         <p className='text-muted' style={{fontSize:'12px'}}>{(new Date(comment.createdAt)).toISOString().slice(0, 19).replace(/-/g, "-").replace("T", " ")}</p>
        //                     </div>
        //                     <div className='col-md-12'>
        //                         <p style={{marginTop: '-10px', wordWrap: "break-word" }}>{comment.content}</p>
        //                     </div>
        //                 </div>
        //             </div>
        //         </div>
        //     ))}
        // </div>
    )
    
    
};

export default CommentList;