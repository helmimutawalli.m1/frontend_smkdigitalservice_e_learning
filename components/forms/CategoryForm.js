import {Button} from 'antd';
import { CloseCircleFilled } from '@ant-design/icons';

const CategoryForm = ({values, setValues, handleSubmit}) => {
    return <div className='container pt-3'>
        <form onSubmit={handleSubmit}>
            <input type='text' className='form-control square' onChange={e => setValues({...values, name: e.target.value})} value={values.name} placeholder='Title' autoFocus required />
            
            <Button onClick={handleSubmit} className='col mt-3'  size='large' type='primary' shape='round'>Save</Button>
        </form>
    </div>
    
};

export default CategoryForm;