import { useState, useEffect } from "react";
import {Select, Button, Avatar, Badge} from 'antd';
import axios from "axios";

const {Option} = Select;

const CourseCreateForm = ({handleSubmit, handleImage, handleChange, values, setValues, preview, uploadButtonText, handleImageRemove = (f) => f, editPage = false}) => {

    // const children = [];
    // for(let i = 1; i <= 10; i++){
    //     const price = i * 100000;
    //     children.push(<Option key={price.toFixed(2)} >Rp.{price.toFixed(2)}</Option>)
    // }

    const [categories, setCategories] = useState([]);
    const [subCategories, setSubCategories] = useState([]);
    
    useEffect(() => {
        if(categories.length == 0 && values) fetchCategory();
        if(subCategories.length == 0 && values) {
            fetchSubCategory(values.category)
        }
    },[values]);

    const fetchCategory = async () => {
        const {data} = await axios.get(`/api/category`);
        if(data) setCategories(data);
    }
    const fetchSubCategory = async (categoryId) => {
        if(categoryId){
            const {data} = await axios.get(`/api/sub-category/${categoryId}`);
            if(data) setSubCategories(data);
        }
    }

    const handleCategoryChange = async (categoryId) => {
        setSubCategories([])
        setValues({...values, category: categoryId, subCategory : ''});

        if(categoryId && categoryId != ''){
            fetchSubCategory(categoryId);
        }
    }

    return (
        <>
            {values && 
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <input type='text' name='name' className="form-control" placeholder="Name" value={values.name} onChange={handleChange} />
                    </div>
                    
                    <div className="form-group">
                        <textarea name="description" cols='7' rows='7' value={values.description} className="form-control" onChange={handleChange} />
                    </div>
                    
                    <div className="form-row">
                        <div className="col">
                            <div className="form-group">
                                <Select style={{width: '100%'}} size='large' value={values.paid} onChange={v => setValues({...values, paid: v, price:0})}>
                                    <Option value={true}>Paid</Option>
                                    <Option value={false}>Free</Option>
                                </Select>
                            </div>
                        </div>

                        {values.paid && 
                            <div className="col-md-3">
                                <div className="form-group">
                                    <input type='number' name='price' style={{width: '100%'}} className="form-control" placeholder="Price" value={values.price} onChange={handleChange} />

                                    {/* <Select defaultValue='Rp.1000000'   onChange={v => setValues({...values, price: v})}
                                    tokenSeparators={[,]} size='large' >
                                        {children}
                                    </Select> */}
                                </div>
                            </div>
                        }
                    </div>
                    <div className="form-row">
                        <div className="col-md-6">
                            <div className="form-group">
                                {/* <input type='text' name='category' className="form-control" placeholder="Category" value={values.category} onChange={handleChange} /> */}
                                <Select style={{width: '100%'}} value={values.category} onChange={v => {handleCategoryChange(v)}}
                                    size='large' >
                                        <Option key={''} >Category</Option>
                                        {categories.map(category => (
                                        <Option key={category._id}>{category.name}</Option>
                                        ))}
                                </Select>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                {/* <pre>{JSON.stringify(subCategories,null,4)}</pre> */}
                                <Select style={{width: '100%'}} value={subCategories && subCategories.length > 0 ? (values.subCategory == '' ? "" : values.subCategory) : "" }   onChange={v => setValues({...values, subCategory: v})}
                                    size='large' >
                                        <Option key={''} >Sub Category</Option>
                                        {subCategories && subCategories.map(subCategory => (
                                        <Option key={subCategory._id}>{subCategory.name}</Option>
                                        ))}
                                </Select>
                            </div>
                        </div>
                    </div>
                    


                    <div className="form-row">
                        <div className="col">
                            <div className="form-group">
                                <label className="btn btn-outline-secondary btn-block text-left">
                                    {uploadButtonText}
                                    <input type='file' name="image" onChange={handleImage} accept="image/*" hidden />
                                </label>
                            </div> 
                        </div>

                        {preview && (
                            <Badge count="X" onClick={handleImageRemove} className='pointer'>
                                <Avatar width={200} src={preview} />
                            </Badge>
                        )}

                        {editPage && values.image && <Avatar width={200} src={values.image.Location} />}
                    </div>
        
                    <div className="row">
                        <div className="col">
                            <Button onClick={handleSubmit} disabled={values.loading || values.uploading} className='btn btn-primary' 
                                type='primary' size="large" shape="round" loading={values.loading}>
                                {values.loading ? "Saving..." : 'Save & Continue'}
                            </Button>
                        </div>
                    </div>
                </form>
            }
        </>
    )
}



export default CourseCreateForm;