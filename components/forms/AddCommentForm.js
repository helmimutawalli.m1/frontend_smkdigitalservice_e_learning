import { Comment, Avatar, Form, Button, List, Input } from 'antd';

const { TextArea } = Input;

const Editor = ({ onChange, onSubmit, submitting, value }) => (
    <>
      <Form.Item>
        <TextArea rows={4} onChange={onChange} value={value} />
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
          Add Comment
        </Button>
      </Form.Item>
    </>
  );

const AddCommentForm = ({values, setValues, handleAddComment, submitting, avatar}) => {
    return <div className='container pt-3 pl-0 pr-0'>
        {/* <form onSubmit={handleAddComment}>
            <input type='text' className='form-control square' onChange={e => setValues({...values, content: e.target.value})} value={values.content} placeholder='Comment...' required />
            <Button onClick={handleAddComment} className='col mt-3'  size='large' type='primary' shape='round'>Add Comment</Button>
        </form> */}
        
        <Comment
          avatar={<Avatar src={avatar} alt={avatar} />}
          content={
            <Editor
              onChange={e => setValues({...values, content: e.target.value})}
              onSubmit={handleAddComment}
              submitting={submitting}
              value={values.content}
            />
          }
        />
    </div>
    
};

export default AddCommentForm;